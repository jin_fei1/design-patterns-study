package com.jcf.api.designpatternsstudy;

import com.jcf.api.designpatternsstudy.abstarctFactory.Provider;
import com.jcf.api.designpatternsstudy.factory.SendFactory;
import com.jcf.api.designpatternsstudy.abstarctFactory.SendMailFactory;
import com.jcf.api.designpatternsstudy.factory.Sender;

@SpringBootTest
class DesignPatternsStudyApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    public void factoryTest(){
        Sender sender = SendFactory.produceMail();
        sender.send();
        Sender sender1 = SendFactory.produceSms();
        sender1.send();
    }
    @Test
    public void abstarctFactoryTest(){
        Provider provider = new SendMailFactory();
        Sender produce = provider.produce();
        produce.send();
    }
}
