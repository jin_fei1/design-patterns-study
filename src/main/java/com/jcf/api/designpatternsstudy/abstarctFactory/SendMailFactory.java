package com.jcf.api.designpatternsstudy.abstarctFactory;
import com.jcf.api.designpatternsstudy.factory.MailSender;
import com.jcf.api.designpatternsstudy.factory.Sender;

public class SendMailFactory implements Provider {
    @Override
    public Sender produce() {
        return new MailSender();
    }
}
