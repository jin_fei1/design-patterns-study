package com.jcf.api.designpatternsstudy.abstarctFactory;
import com.jcf.api.designpatternsstudy.factory.Sender;
import com.jcf.api.designpatternsstudy.factory.SmsSender;

public class SendSmsFactory implements Provider {
    @Override
    public Sender produce() {
        return new SmsSender();
    }
}
