package com.jcf.api.designpatternsstudy.abstarctFactory;

import com.jcf.api.designpatternsstudy.factory.Sender;

public interface Provider {
    Sender produce();
}
