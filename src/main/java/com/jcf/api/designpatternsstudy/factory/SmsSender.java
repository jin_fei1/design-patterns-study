package com.jcf.api.designpatternsstudy.factory;

public class SmsSender implements Sender{
    @Override
    public void send() {
        System.out.println("短信发送");
    }
}
