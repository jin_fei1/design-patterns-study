package com.jcf.api.designpatternsstudy.factory;

public class MailSender implements Sender{
    @Override
    public void send() {
        System.out.println("邮件发送");
    }
}
