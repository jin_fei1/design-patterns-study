package com.jcf.api.designpatternsstudy.factory;

public interface Sender {
    void send();
}
